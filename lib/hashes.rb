# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  hsh = {}
  str.split.each { |word| hsh[word] = word.length }
  hsh
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)

  hash.sort_by{ |k, v| v}[-1][0]

end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each { |k, v| older[k] = v }
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  hsh = Hash.new(0)
  word.each_char { |ch| hsh[ch] += 1 }
  hsh
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  hsh = {}
  arr.each { |el| hsh[el] = 1 }
  hsh.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  parity_hash = Hash.new(0)
  numbers.each do |num|
    if num.even?
      parity_hash[:even] += 1
    else
      parity_hash[:odd] += 1
    end
  end
  parity_hash
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowel_count = Hash.new(0)
  vowels = %w(a e i o u)
  string.each_char do |ch|
    vowel_count[ch] += 1 if vowels.include?(ch)
  end
  max_val(vowel_count)
end

def max_val(hash)
  arr = hash.sort_by { |_k, v| v }
  max = arr[-1][1]
  return arr[-1][0] unless hash.values.count(arr[-1][1]) < 1
  alphabetize_max_val(arr, max)[0][0]
end

def alphabetize_max_val(arr, max)
  alpha = arr.select { |arr1| arr1[0] == max }
  alpha.sort_by! { |arr3| arr3[0] }
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  second_half_birthdays = []
  students.each do |k, v|
    second_half_birthdays << k if second_half_birthday?(v)
  end
  # make new array with everything else
  combine_names(second_half_birthdays)
end

def second_half_birthday?(month)
  return true if month > 6
  false
end

def combine_names(names)
  combinations = []
  names.each_with_index do |el, idx|
    (idx + 1...names.length).each do |num|
      combinations << [el, names[num]]
    end
  end
  combinations
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  species = Hash.new(0)
  specimens.each { |sp| species[sp] += 1 }
  calc_index(species)
end

def calc_index (species)
  populations = species.values
  species.length**2 * populations.min / populations.max
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  alpha = ("a".."z")
  clean_sign = normal_sign.chars.select { |ch| alpha.cover?(ch.downcase) }.join
  normal_sign_count = character_count(clean_sign)
  vandalized_sign.each_char do |ch|
    next if ch == ' '
    normal_sign_count[ch.downcase] -= 1
    return false if normal_sign_count[ch] < 0
  end
  true
end

def character_count(str)
  hash = Hash.new(0)
  str.each_char { |ch| hash[ch] += 1 }
  hash
end
